import 'dart:developer';
import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_instagram/models/user_model.dart';
import 'package:flutter_instagram/services/database_service.dart';
import 'package:flutter_instagram/services/storage_service.dart';
import 'package:image_picker/image_picker.dart';

class EditProfileScreen extends StatefulWidget {
  
  final User user;

  EditProfileScreen({this.user});
  
  @override
  _EditProfileScreenState createState() => _EditProfileScreenState();
}

class _EditProfileScreenState extends State<EditProfileScreen> {
  
  final _formKey = GlobalKey<FormState>();
  File _profileImage;
  String _name = '';
  String _bio = ''; 
  bool _isLoading = false;

  @override
  void initState() {
    super.initState();
    _name = widget.user.name;
    _bio = widget.user.bio;  
  }

  //iOS 카메라 접근을 위해 Runner info.plist - Photo Library Usage / Camera Usage 설정
  _handleImageFromGallery() async {
    File imageFile = await ImagePicker.pickImage(source: ImageSource.gallery);
    if (imageFile != null) {
      setState(() {
        _profileImage = imageFile;
      });
    }
  }

  _displayProfileImage() {
    //갤러리 에서 사진이 선택된게 아니라면
    if (_profileImage == null) {
      //user 안에 사진 데이터가 없다면
      if (widget.user.profileImageUrl.isEmpty) {
        //기본 빈 사진 이미지
        return AssetImage('assets/images/user_placeholder.jpg');
      //user 안에 사진 데이터가 있다면
      } else {
        //url 이미지 이미지로 전환
        return CachedNetworkImageProvider(widget.user.profileImageUrl); 
      }
    } else {
      log('data: $_profileImage');
      //이미지파일 이미지로 전환
      return FileImage(_profileImage);
    }
  } 
  
  _submit() async {
    //save profile 버튼이 눌린다.
    if (_formKey.currentState.validate()) {
      //TextFormField widget 에서 저장했던 값을 setState 해준다.
      _formKey.currentState.save();
      //데이터가 업로드 될 예정이므로 _isLoading 을 true 로 해준다.
      setState(() {
        _isLoading = true;
      });
      //Update user in database
      String _profileImageUrl = '';
      //기계의 사진첩에서 가져온 사진이 없다면
      if (_profileImage == null) {
        //기존 사용했던 imageUrl 을 사용한다.
        _profileImageUrl = widget.user.profileImageUrl;
      } else {
        //기계의 사진첩에서 가져온 사진의 데이터와, 유저 프로파일 이미지 url 넣어 새 ImageUrl 을 가져온다.
        _profileImageUrl = await StorageService.uploadUserProfileImage(
          widget.user.profileImageUrl,
          _profileImage
          );
      }

      User user = User(
        id: widget.user.id,
        name: _name,
        profileImageUrl: _profileImageUrl,
        bio: _bio
        );
      // Database update
      DatabaseService.updateUser(user);
      Navigator.pop(context);
    }
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text('Edit Profile',
         style: TextStyle(color: Colors.black), 
         ),
      ),

      body: GestureDetector(
        onTap: () => FocusScope.of(context).unfocus(), 
          child: ListView(
          children: <Widget>[
            _isLoading 
            ? LinearProgressIndicator(
              backgroundColor: Colors.blue[200],
              valueColor: AlwaysStoppedAnimation(Colors.blue),
            )
            : SizedBox.shrink(),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Form(
                key: _formKey,
                child: Column(
                  children: <Widget>[
                    CircleAvatar(
                      radius: 60.0,
                      backgroundColor: Colors.grey,
                      backgroundImage: 
                        _displayProfileImage(),
                    ),

                    FlatButton(
                      onPressed: () => _handleImageFromGallery(),
                      child: Text(
                        'Change Profile Image',
                        style: TextStyle(
                          color: Theme.of(context).accentColor, fontSize: 16.0),
                      ),
                    ),

                    TextFormField(
                      initialValue: _name,
                      style: TextStyle(fontSize: 18.0),
                      decoration: InputDecoration(
                        icon: Icon(
                          Icons.person,
                          size: 30.0
                        ),
                        labelText: 'Name',
                        ),
                        validator: (input) => input.trim().length < 1 ? 'Please enter a valid name' : null,
                        onSaved: (input) => _name = input,
                    ),

                    TextFormField(
                      initialValue: _bio,
                      style: TextStyle(fontSize: 18.0),
                      decoration: InputDecoration(
                        icon: Icon(
                          Icons.book,
                          size: 30.0
                        ),
                        labelText: 'Bio',
                        ),
                        validator: (input) => input.trim().length > 150 ? 'Please enter a bio less than 150 characters' : null,
                        onSaved: (input) => _bio = input,
                    ),

                    Container(
                      margin: EdgeInsets.all(40.0),
                      height: 40.0,
                      width: 250.0,
                      child: FlatButton(
                        onPressed: () => _submit(),
                        color: Colors.blue,
                        textColor: Colors.white,
                        child: Text(
                          'Save Profile',
                          style: TextStyle(fontSize: 18.0),
                        ),
                      ),
                    )
                  ],
                 ),
                ),
            ),
          ],
          ),
      ),
    ); 
  }
} 