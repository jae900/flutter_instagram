import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_instagram/models/user_data.dart';
import 'package:flutter_instagram/screens/create_post_screen.dart';
import 'package:flutter_instagram/screens/feed_screen.dart';
import 'package:flutter_instagram/screens/profile_screen.dart';
import 'package:flutter_instagram/screens/search_scrren.dart';
import 'package:flutter_instagram/screens/activity_screen.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatefulWidget {

  @override
  _HomeScreenState createState() => _HomeScreenState();

}

class _HomeScreenState extends State<HomeScreen> {
  
  int _currentTab = 0;
  PageController _pageController;
  
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _pageController = PageController();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        controller: _pageController,
        children: <Widget>[
          FeedScreen(),
          SearchScreen(),
          CreatePostScreen(),
          ActivityScreen(),
          //Provider 를 이용해 안의 변수를 static 처럼 사용할 수 있는듯
          ProfileScreen(userId: Provider.of<UserData>(context).currentUserId), 
        ],
        onPageChanged: (int index) {
          setState(() { 
            _currentTab = index;
          });
        },
      ),


      bottomNavigationBar: CupertinoTabBar(
        currentIndex: _currentTab,
        onTap: (int index) {
          setState(() {
            _currentTab = index;
            print(_currentTab);
          });
 
          _pageController.animateToPage(
            index,
            duration: Duration(milliseconds: 200),
            curve: Curves.easeIn,
          );
        },
        activeColor: Colors.black,
        items: [
          BottomNavigationBarItem(
            icon: Icon(
              Icons.home,
              size: 32.0,
             )
          ),

          BottomNavigationBarItem(
            icon: Icon(
              Icons.search,
              size: 32.0,
             )
          ),

          BottomNavigationBarItem(
            icon: Icon(
              Icons.photo_camera,
              size: 32.0,
             )
          ),

          BottomNavigationBarItem(
            icon: Icon(
              Icons.notifications,
              size: 32.0,
             )
          ),

          BottomNavigationBarItem(
            icon: Icon(
              Icons.account_circle ,
              size: 32.0,
             )
          )
        ],
      ),
    );
  }
}