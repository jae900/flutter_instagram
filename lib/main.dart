import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_instagram/models/user_data.dart';
import 'package:flutter_instagram/screens/home_screen.dart';
import 'package:flutter_instagram/screens/login_screen.dart';
import 'package:flutter_instagram/screens/signup_screen.dart';
import 'package:flutter_instagram/screens/feed_screen.dart';
import 'package:provider/provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  Widget _getScreenId() {
    return StreamBuilder<FirebaseUser>(
      stream: FirebaseAuth.instance.onAuthStateChanged,
      builder: (BuildContext context, snapshot) {
        if (snapshot.hasData) {
          Provider.of<UserData>(context).currentUserId = snapshot.data.uid;
          return HomeScreen();
        } else {
          return LoginScreen();
        }
      }
    );
  }

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    
    return ChangeNotifierProvider(
        builder: (context) => UserData(),
        child: MaterialApp(
          title: 'Instagram Clone',
          theme: ThemeData(
          //기본 아이콘
          primaryIconTheme: Theme.of(context).primaryIconTheme.copyWith(
            color: Colors.black
          ),
        ),
        debugShowCheckedModeBanner: false,
        home: _getScreenId(),
        /*
        Swift 에 UINavigationContoller 가 Stack 에 쌓여있는 UIViewController 를 관리 했듯이 flutter 에도 비슷한 
        기능이 있다. Routes 와 navigator 인데 Routes 에 Screen 을 쌓아두고 Navigator 로 관리 한다.
        */
        routes: {
          LoginScreen.id: (context) => LoginScreen(),
          SignupScreen.id: (context) => SignupScreen(),
          FeedScreen.id: (context) => FeedScreen()
        },
      ),
    );
  }
}


