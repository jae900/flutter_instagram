import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_instagram/models/user_data.dart';
import 'package:flutter_instagram/screens/feed_screen.dart';
import 'package:flutter_instagram/screens/login_screen.dart';
import 'package:provider/provider.dart';

class AuthService {
  static final _auth = FirebaseAuth.instance;
  static final _firestore = Firestore.instance;

  static void signUpUser(BuildContext context, String name, String email, String password) async {

    try {
      AuthResult authResult = await _auth.createUserWithEmailAndPassword(
        email: email,
        password: password
      );

      FirebaseUser signedInUser = authResult.user;
      if (signedInUser != null) {
        _firestore.collection('/users').document(signedInUser.uid).setData({
          'name': name,
          'email': email,
          'profileImageUrl': ''
          });
          //회원가입 할 때 회원가입된 uid 를 provider 를 이용해 저장
          Provider.of<UserData>(context).currentUserId = signedInUser.uid;
          // Navigator.pushReplacementNamed(context, FeedScreen.id);
          Navigator.pop(context);
      }
    } catch (err) {
      print(err);
    }

  }

  static void logout(BuildContext context) {
    _auth.signOut(); 
    // Navigator.pushReplacementNamed(context, LoginScreen.id);
  } 

  static void login(BuildContext context, String email, String password) async {
    try {
      await _auth.signInWithEmailAndPassword(email: email, password: password);
      // Navigator.pushReplacementNamed(context, FeedScreen.id);
    } catch (error) {
      print(error);
    }
  }
}