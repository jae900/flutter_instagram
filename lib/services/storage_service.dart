import 'dart:core' as prefix0;
import 'dart:core';
import 'dart:io';
import 'dart:developer';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:flutter_instagram/utilites/constrants.dart';
import 'package:path_provider/path_provider.dart';
import 'package:uuid/uuid.dart';

class StorageService {
  
  static Future<String> uploadUserProfileImage(String url, File imageFile) async {
    //기존 사용 image url 과 변경한 새 image 를 입력받는다.
    //photoId 를 만든다.
    String photoId = Uuid().v4();
    //image 를 압축한다.
    File image = await compressImage(photoId, imageFile);

    //한번 저장했던 이력이 있다면 덮어씌운다.
    //이전 photoId 를 찾아 사용 한다.
    if (url.isNotEmpty) {
      //userProfile_(.*).jpg 을 찾는 키로 한다.
      RegExp exp = RegExp(r'userProfile_(.*).jpg');
      //url 안의 RegExp 키를 찾는다. 찾은 id 를 photoId 로 한다.
      photoId = exp.firstMatch(url)[1];
    }
    //firebase Storage 에 image 를 업로드 한다.
    StorageUploadTask uploadTask = storageRef.child('images/users/userProfile_$photoId.jpg').putFile(image);
    //Storage 에 업로드 된 이미지의 url 을 가져온다.
    StorageTaskSnapshot storageSnap = await uploadTask.onComplete;
    String downloadUrl = await storageSnap.ref.getDownloadURL();
    //url 을 반환한다.
    return downloadUrl;
  }

  static Future<File> compressImage(String photoId, File image) async {
    final tempDir = await getTemporaryDirectory();
    final path = tempDir.path;
    //path: 원본 이미지 위치, targetPath: 압축된 이미지 저장위치
    File compressedImageFile = await FlutterImageCompress.compressAndGetFile(
      image.absolute.path,
      '$path/img_$photoId.jpg',
      quality: 70
      );
    return compressedImageFile;
  }

  static Future<String> uploadPost(File imageFile) async {
    String photoId = Uuid().v4();
    File image = await compressImage(photoId, imageFile);
    StorageUploadTask uploadTask = storageRef.child('images/users/userProfile_$photoId.jpg').putFile(image);
    StorageTaskSnapshot storageSnap = await uploadTask.onComplete;
    String downloadUrl = await storageSnap.ref.getDownloadURL();
    return downloadUrl;
  }


}
